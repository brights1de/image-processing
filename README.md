# Image Processing

8th semester NUST MISIS Applied Math course

## How to run

Build and make via cmake
run run.sh file in your runtime directory

```cmd
mkdir build
cd build
cmake ../
make
cd ../bin
sh run.sh
```
