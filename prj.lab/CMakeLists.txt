add_subdirectory(lab01)
add_subdirectory(lab02)
add_subdirectory(lab03)
add_subdirectory(lab04)

configure_file("run.sh" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/run.sh" COPYONLY)