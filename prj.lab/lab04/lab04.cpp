#include <opencv2/opencv.hpp>

static const int d = 100;
static const cv::Rect2i rc_cell{0, 0, d, d};

void draw_cell(cv::Mat& img, const int32_t x, const int32_t y, 
  const cv::Scalar color_ground, const cv::Scalar color_figure) {
  cv::Rect2i rc = rc_cell;
  rc.x += x;
  rc.y += y;
  cv::rectangle(img, rc, color_ground, -1);
  cv::circle(img, {rc.x + rc.width / 2, rc.y + rc.height / 2}, rc.width / 5, color_figure, -1);
}

int main() {
  cv::Mat img_src(200, 300, CV_8UC1);
  cv::Mat img_res1 = img_src.clone();
  cv::Mat img_res2 = img_src.clone();


  // draw source image
  draw_cell(img_src, 0, 0, { 255 }, { 127 });
  draw_cell(img_src, d, 0, { 127 }, { 0 });
  draw_cell(img_src, d * 2, 0, { 0 }, { 255 });
  draw_cell(img_src, 0, d, { 0 }, { 127 });
  draw_cell(img_src, d, d, { 255 }, { 0 });
  draw_cell(img_src, d * 2, d, { 127 }, { 255 });
  cv::imwrite("./retval/lab04.src.png", img_src);


  //create 1st kernel
  cv::Mat_<float> kernel1(3,3);
  cv::Mat_<float> kernel2(3,3);

  //blur
  kernel1 << 1.0/9, 1.0/9, 1.0/9, 1.0/9, 1.0/9, 1.0/9, 1.0/9, 1.0/9, 1.0/9;
  
  //something
  kernel2 << 0.0, 0.0, 2.0, -5.0, 5.0, -5.0, 2.0, 0.0, 0.0;

  cv::filter2D(img_src, img_res1, CV_32F, kernel1);
  cv::filter2D(img_src, img_res2, CV_32F, kernel2);
  

  cv::imwrite("./retval/lab04.viz_dx.png", img_res1);
  cv::imwrite("./retval/lab04.viz_dy.png", img_res2);

  // task 3
  cv::Mat result;
  cv::pow(img_res1, 2, img_res1);
  cv::pow(img_res2, 2, img_res2);
  cv::pow((img_res1 + img_res2), 0.5, result);
  
  

  cv::imwrite("./retval/lab04.viz_gradmod.png", result);

  return 0;
}