#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>

// function to get one channel image
cv::Mat getOneColor(cv::Mat& rhs_mat, int color)
{
  cv::Mat retval;
  cv::Mat chans[3];
  cv::split(rhs_mat, chans);
  for (size_t i = 0; i < 3; i++)
  {
    if (color != i)
    {
      chans[i] = cv::Mat::zeros(rhs_mat.rows, rhs_mat.cols, CV_8UC1); // green channel is set to 0
    }
  }
  cv::merge(chans, 3, retval);
  return retval;
}

int main() 
{
  cv::Mat img_src = cv::imread("../testdata/cross_0256x0256.png");
  cv::Mat img_res = img_src.clone();

  // draw images with fixed quality
  cv::imwrite("./retval/lab02.95.jpg", img_res, {cv::IMWRITE_JPEG_QUALITY, 95});
  cv::imwrite("./retval/lab02.65.jpg", img_res, {cv::IMWRITE_JPEG_QUALITY, 65});

  // read imgs
  cv::Mat img_95 = cv::imread("./retval/lab02.95.jpg");
  cv::Mat img_65 = cv::imread("./retval/lab02.65.jpg");

  // create array of images for easier processing
  std::vector<cv::Mat>imgs = {img_res, img_95, img_65};
  std::vector<std::vector<cv::Mat>>splits;
  cv::Mat diff;
  cv::Mat grey_diff;

  // split each img in channels grey
  for (int i = 0; i < 3; i++)
  {
    std::vector<cv::Mat>bgrchannels(3);
    cv::split(imgs[i], bgrchannels);
    splits.push_back(bgrchannels);
  }

  // small processing to make mosaic
  std::vector<cv::Mat> concat(3);
  for (int i = 0; i < 3; i++)
  {
    cv::hconcat(splits[i], concat[i]);
  }
  // count difference
  grey_diff = cv::abs(concat[2] - concat[1])*30;

  // make mosaic
  cv::Mat mosaic_channels_grey;
  cv::vconcat(concat, mosaic_channels_grey);

  // WRITE
  cv::imwrite("./retval/lab02.mosaic_channels_grey.png", mosaic_channels_grey);
  cv::imwrite("./retval/lab02.grey_diff.png", grey_diff);


  // clear splits to rewrite it
  splits.clear();

  // split each img in channels colored
  for (int i = 0; i < 3; i++)
  {
    std::vector<cv::Mat>bgrchannels(3);
    for (int j = 0; j < 3; j++)
    {
      bgrchannels[j] = getOneColor(imgs[i], j);
    }
    splits.push_back(bgrchannels);
  }

  // small processing to make mosaic colored
  concat = std::vector<cv::Mat>(3);
  for (int i = 0; i < 3; i++)
  {
    cv::hconcat(splits[i], concat[i]);
  }

  // count diff
  diff = cv::abs(concat[2] - concat[1]) * 30;
  cv::Mat mosaic_channels_colored;
  cv::vconcat(concat, mosaic_channels_colored);

  cv::imwrite("./retval/lab02.mosaic_channels_colored.png", mosaic_channels_colored);
  cv::imwrite("./retval/lab02.diff.png", diff);

  
  return 0;
}
