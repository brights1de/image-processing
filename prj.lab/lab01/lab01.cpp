#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

using namespace std;

int main()
{
  // create picture
  cv::Mat image = cv::Mat(cv::Size(3 * 256, 60 * 4), CV_8UC1);

  // fill with gray color
  int color = 0;
  for (size_t i = 0; i < image.cols / 3; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      image.col(i * 3 + j).setTo(color);
    }
    color++;
  }

  // via roi 
  cv::Rect roi_1 = cv::Rect(0, 0, 3 * 256, 60);
  cv::Rect roi_2 = cv::Rect(0, 60, 3 * 256, 60);
  cv::Mat img_roi_2_cloned = image(roi_2).clone();
  cv::Mat img_roi_2 = image(roi_2);

  // gamma correction itself, convert image to float [0,1] to pow() it, if we will precced pow() under int [0, 255] we can easy get overflow (30**2 = 900)
  cv::Mat mat_float, newhammafloat;
  img_roi_2_cloned.convertTo(mat_float, CV_32FC1, (float)1.0 / 255.0);
  cv::pow(mat_float, 2.4, newhammafloat);

  // back to [0, 255]
  newhammafloat.convertTo(img_roi_2_cloned, CV_8UC1, 255.0);

  // copy gamma-corrected picture to our area on big image
  img_roi_2_cloned.copyTo(img_roi_2);
  

  //task b
  cv::Rect roi_3 = cv::Rect(0, 120, 3 * 256, 60);
  cv::Rect roi_4 = cv::Rect(0, 180, 3 * 256, 60);

  cv::Mat mat3 = image(roi_3);
  cv::Mat mat4 = image(roi_4);


  color = 5;
  for (size_t i = 0; i < image.cols; i++)
  {
    if(i%30 == 0)
    {
      color+=10;
    }
    mat3.col(i).setTo(color);
  }


  mat3.convertTo(mat_float, CV_32FC1, (float)1.0 / 255.0);
  cv::pow(mat_float, 2.4, newhammafloat);
  // back to [0, 255]
  cv::Mat mat4_cloned = mat4.clone(); 
  newhammafloat.convertTo(mat4_cloned, CV_8UC1, 255.0);

  // copy gamma-corrected picture to our area on big image
  mat4_cloned.copyTo(mat4);

  cv::imwrite("./retval/lab01.png", image);
  return 0;
}