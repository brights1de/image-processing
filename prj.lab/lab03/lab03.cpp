#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>


// generate lookuptable for solarisation
cv::Mat generateLUTsolarisation()
{
  auto func = [](int x){return 4*1.0*x*(255.0-x)/255.0;};
  cv::Mat table(1, 256, CV_8U);
  for (int i = 0; i < 256; i++)
  {
    table.at<uchar>(0, i) = func(i);
  }
  return table;
}

// solarize image
cv::Mat solarisation(cv::Mat& rhs_img)
{
  cv::Mat retval;
  cv::Mat table = generateLUTsolarisation();
  cv::LUT(rhs_img, table, retval);
  
  return retval;
}


cv::Mat createHist(cv::Mat& img_src)
{
  // for hist we will use opencv calcHist function
  int histSize = 256;
  cv::Mat hist;
  float range[] = { 0, 256 }; //the upper boundary is exclusive
  const float* histRange = { range };


  int hist_w = 512, hist_h = 400;
  cv::Mat histImage(hist_h, hist_w, CV_8UC1, cv::Scalar(0,0,0));
  cv::calcHist(&img_src, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, true, false );
  cv::normalize(hist, hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat() );

  return hist;
}


cv::Mat visualizeHist(cv::Mat& hist)
{
  int hist_w = 512, hist_h = 400;
  int histSize = 256;

  int bin_w = cvRound( (double) hist_w/histSize ); 
  cv::Mat histImage(hist_h, hist_w, CV_8UC1, cv::Scalar(0,0,0));
  for( int i = 1; i < histSize; i++ )
  {
      cv::line( histImage, cv::Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ),
            cv::Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
            cv::Scalar( 255, 0, 0), 2, 8, 0  );
  }
  return histImage;
}

int main() 
{
  // read img
  // cv::Mat img_src = cv::imread("../testdata/cross_0256x0256.png", cv::ImreadModes::IMREAD_GRAYSCALE);
  cv::Mat img_src = cv::imread("../testdata/colosseum.jpg", cv::ImreadModes::IMREAD_GRAYSCALE);

  auto hist_src = createHist(img_src);
  auto histImage = visualizeHist(hist_src);


  // write first two tasks
  cv::imwrite("./retval/lab03.src.png", img_src);
  cv::imwrite("./retval/lab03.hist.src.png", histImage );

  // generate out LUT table and make it look like 2d map to write it
  cv::Mat sol_line = generateLUTsolarisation();
  cv::Mat LUTgraph(256, 256, CV_8UC1, cv::Scalar(0));
  for (size_t i = 0; i < 256; i++)
  {
    LUTgraph.at<uchar>(sol_line.at<uchar>(i), i) = 255;
  }
  cv::flip(LUTgraph, LUTgraph, 0);
  

  
  // write lut
  cv::imwrite("./retval/lab03.lut.png", LUTgraph);

  // apply LUT to src image
  cv::Mat solarisation_image = solarisation(img_src);
  cv::imwrite("./retval/lab03.lut.src.png", solarisation_image);

  // count hist on solarisation_image, we will rewrite
  cv::Mat hist_solarisation;
  hist_solarisation = createHist(solarisation_image);
  auto histImage_solarisation = visualizeHist(hist_solarisation);

  // write hist of solarisationed image
  cv::imwrite("./retval/lab03.hist.lut.src.png", histImage_solarisation);


  //CLAHE
  cv::Mat clahe1, clahe2, clahe3;
  cv::createCLAHE(20, cv::Size(15, 15))->apply(img_src, clahe1);
  cv::createCLAHE(30, cv::Size(15, 15))->apply(img_src, clahe2);
  cv::createCLAHE(50, cv::Size(3, 3))->apply(img_src, clahe3);

  auto clahe1_hist = createHist(clahe1);
  auto clahe1_hist_vis = visualizeHist(clahe1_hist);

  auto clahe2_hist = createHist(clahe2);
  auto clahe2_hist_vis = visualizeHist(clahe2_hist);

  auto clahe3_hist = createHist(clahe3);
  auto clahe3_hist_vis = visualizeHist(clahe3_hist);

  cv::imwrite("./retval/lab03.clahe.1.png", clahe1);
  cv::imwrite("./retval/lab03.hist.clahe.1.png", clahe1_hist_vis);
  cv::imwrite("./retval/lab03.clahe.2.png", clahe2);
  cv::imwrite("./retval/lab03.hist.clahe.2.png", clahe2_hist_vis);
  cv::imwrite("./retval/lab03.clahe.3.png", clahe3);
  cv::imwrite("./retval/lab03.hist.clahe.3.png", clahe3_hist_vis);
  
  // global binarization
  cv::Mat global_binarization;
  cv::threshold(img_src, global_binarization, 50, 200, cv::THRESH_BINARY);
  cv::Mat src_and_global_binarisation;
  cv::hconcat(std::vector<cv::Mat>({img_src, global_binarization}), src_and_global_binarisation);
  cv::imwrite("./retval/lab03.bin.global.png", src_and_global_binarisation);


  // global binarization
  cv::Mat local_binarization;
  cv::threshold(img_src, local_binarization, 50, 200, cv::ADAPTIVE_THRESH_GAUSSIAN_C);
  cv::Mat src_and_local_binarisation;
  cv::hconcat(std::vector<cv::Mat>({img_src, local_binarization}), src_and_local_binarisation);
  cv::imwrite("./retval/lab03.bin.local.png", src_and_local_binarisation);


  //morphological filtration
  cv::Mat morphological_filtration;
  cv::Mat kernel = cv::Mat::ones(5,5, CV_8U);
  cv::morphologyEx(img_src, morphological_filtration, cv::MORPH_GRADIENT, kernel);
  cv::Mat src_and_morphological_filtration;
  cv::hconcat(std::vector<cv::Mat>({img_src, morphological_filtration}), src_and_morphological_filtration);
  cv::imwrite("./retval/lab03.morph.png", src_and_morphological_filtration);

  

  // binary mask visualisation
  cv::Mat binary_mask;
  double aplha = 0.6, beta = 0.4;
  cv::addWeighted(global_binarization, aplha, img_src, beta, 0.0, binary_mask);

  cv::imwrite("./retval/lab03.mask.png", binary_mask);

  return 0;
}